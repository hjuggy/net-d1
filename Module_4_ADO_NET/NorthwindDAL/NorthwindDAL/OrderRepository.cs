﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using NorthwindDAL.Contract;

namespace NorthwindDAL
{

    public class OrderRepository : IOrderRepository
    {
        private readonly DbProviderFactory ProviderFactory;
        private readonly string ConnectionString;

        public OrderRepository(string connectionString, string provider)
        {
            ProviderFactory = DbProviderFactories.GetFactory(provider);
            ConnectionString = connectionString;
        }

        public virtual IEnumerable<Order> GetOrders()
        {
            var resultOrders = new List<Order>();

            using (var connection = ProviderFactory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "select OrderID " +
                                                    ",CustomerID " +
                                                    ",EmployeeID " +
                                                    ",OrderDate " +
                                                    ",RequiredDate "+
                                                    ",ShippedDate " +
                                                    ",ShipVia " +
                                                    ",Freight " +
                                                    ",ShipName " +
                                                    ",ShipAddress " +
                                                    ",ShipCity " +
                                                    ",ShipRegion " +
                                                    ",ShipPostalCode " +
                                                    ",ShipCountry " +
                                                    ",CASE " +
                                                        "WHEN OrderDate IS  NULL THEN 1 " +
                                                        "WHEN ShippedDate IS NULL THEN 2 " +
                                                        "WHEN ShippedDate IS NOT NULL THEN 3 " +
                                                        "END as 'Status' " +
                                                    "from Orders";
                    command.CommandType = CommandType.Text;

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var order = new Order();
                            order.OrderID = reader.GetInt32(0);
                            order.CustomerID = reader.GetString(1);
                            order.EmployeeID = reader.GetInt32(2);
                            order.OrderDate = reader.GetFieldValue<DateTime?>(3);
                            order.RequiredDate = reader.GetFieldValue<DateTime?>(4);
                            if (reader["ShippedDate"] is DateTime)
                            {
                                order.ShippedDate = (DateTime) reader["ShippedDate"];
                            }
                            else
                            {
                                order.ShippedDate = null;
                            }
                            order.ShipVia = reader.GetInt32(6);
                            order.Freight = reader.GetDecimal(7);
                            order.ShipName = reader.GetString(8);
                            order.ShipAddress = reader.GetString(9);
                            order.ShipCity = reader.GetString(10);
                            // order.ShipRegion = reader.GetFieldValue<char[]>(11);
                            //order.ShipPostalCode = reader.GetString(12);
                            order.ShipCountry = reader.GetString(13);
                            order.Status = (StatusOrder) reader.GetInt32(14);
                            resultOrders.Add(order);
                        }
                    }
                }
            }

            return resultOrders;
        }



        public Order GetOrderById(int id)
        {
            using (var connection = ProviderFactory.CreateConnection())
            {
                connection.ConnectionString = ConnectionString;
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = 
                        "select OrderID, OrderDate from dbo.Orders where OrderID = @id; " +
                        "select * from dbo.[Order Details] where OrderID = @id";
                    command.CommandType = CommandType.Text;

                    var paramId = command.CreateParameter();
                    paramId.ParameterName = "@id";
                    paramId.Value = id;

                    command.Parameters.Add(paramId);

                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.HasRows) return null;

                        reader.Read();

                        var order = new Order();
                        order.OrderID = reader.GetInt32(0);
                        order.OrderDate = reader.GetDateTime(1);

                        reader.NextResult();
                        order.Details = new List<OrderDetail>();

                        while (reader.Read())
                        {
                            var detail = new OrderDetail();
                            detail.UnitPrice = (decimal)reader["unitPrice"];
                            detail.Quantity = (int)reader["Quantity"];

                            order.Details.Add(detail);
                        }

                        return order;
                    }
                }
            }
        }

        public IEnumerable<Order> GetOrdersByOrderDate(DateTime orderDate)
        {
            throw new NotImplementedException();
        }

        public Order AddNew(Order newOrder)
        {
            throw new NotImplementedException();
        }

        public Order Update(Order order)
        {
            throw new NotImplementedException();
        }
    }
}
