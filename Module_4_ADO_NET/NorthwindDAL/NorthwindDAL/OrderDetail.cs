﻿namespace NorthwindDAL
{
    public class OrderDetail
    {
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
    }
}