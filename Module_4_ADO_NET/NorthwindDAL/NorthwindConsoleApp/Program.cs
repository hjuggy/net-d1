﻿using NorthwindDAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthwindConsoleApp
{
    class Program
    {
       static void Main(string[] args)
       {
           var northwindConnection = ConfigurationManager.ConnectionStrings["NorthwindSQLConnection"];
           var connectionString = northwindConnection.ConnectionString;
           var providerName = northwindConnection.ProviderName;

           var dal = new OrderRepository(connectionString, providerName);

           var orders = dal.GetOrders();

           foreach (var order in orders)
           {
               Console.WriteLine($"{order.OrderID} {order.Status.ToString()} {order.ShippedDate.ToString()}");
           }

           Console.ReadKey();
       }
    }
}
