﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqTask.Data;

namespace LinqTask
{
    public class LinqTasks
    {
        private DataSource _dataSource = new DataSource();

        /// <summary>
        /// Task1
        /// </summary>
        public IEnumerable<Customer> GetCustomersByAmountMoreThreshold(decimal threshold) =>
            _dataSource.Customers.Where(p => p.Orders.Sum(o => o.Total) > threshold);

        /// <summary>
        /// Task2
        /// </summary>
        public IEnumerable<Supplier> GetSuppliersInCityOfCustomer(Customer customer) =>
            _dataSource.Suppliers.Where(s => s.Country == customer.Country && s.City == customer.City);
        
        /// <summary>
        /// Task3
        /// </summary>
        public IEnumerable<Customer> GetCustomersByOrderMoreThreshold(decimal threshold) =>
            _dataSource.Customers.Where(p => p.Orders.Any(o => o.Total > threshold));

        /// <summary>
        /// Task4
        /// </summary>
        public IEnumerable<ResultTask4> GetCustomersWithDateFirstOrder() =>
            _dataSource.Customers.Where(p => p.Orders.Length > 0)
                                 .Select(c => new ResultTask4 {Customer = c, FirstOrder = c.Orders.Min(p => p.OrderDate)});

        /// <summary>
        /// Task5
        /// </summary>
        public IEnumerable<ResultTask4> GetCustomersWithDateFirstOrderSort() =>
            _dataSource.Customers.Where(p => p.Orders.Length > 0)
                                 .Select(c => new ResultTask4 { Customer = c, FirstOrder = c.Orders.Min(p => p.OrderDate)})
                                 .OrderBy(p=>p.FirstOrder.Year)
                                 .ThenBy(p=>p.FirstOrder.Month)
                                 .ThenBy(p => p.FirstOrder.Date)
                                 .ThenByDescending(p=>p.Customer.CompanyName);

        /// <summary>
        /// Task6
        /// </summary>
        public IEnumerable<Customer> GetCustomersWithSpecialFilling() =>
            _dataSource.Customers.Where(p =>
                !(p.PostalCode is int)
                || string.IsNullOrWhiteSpace(p.Region)
                || p.Phone[0] != '(');


        /// <summary>
        /// Task7
        /// </summary>
        public IEnumerable<ResultTask7> GetProduct()
        {
            return _dataSource.Products.GroupBy(p => p.Category,
                (k, products) => new ResultTask7
                {
                    Key = k,
                    GroupProducts = products.GroupBy(p => p.UnitsInStock,
                        (uis, pods) => new ResultTask7.SubGroup
                        {
                            Key = uis,
                            Products = pods.OrderBy(s=>s.UnitPrice)
                        })
                });
        }

        /// <summary>
        /// Task8
        /// </summary>
        public IEnumerable<ResultTask8> GetProductByPrice()
        {
            string catPrice;
            var ranges = new List<decimal> {10, 20, decimal.MaxValue};
            return _dataSource.Products
                .Select(q =>
                {
                    if (q.UnitPrice <= ranges[0])
                    {
                        catPrice = "Low";
                    }
                    else if (q.UnitPrice <= ranges[2] && q.UnitPrice > ranges[1])
                    {
                        catPrice = "High";
                    }
                    else
                    {
                        catPrice = "Medium";
                    }

                    return new {categoryPrice = catPrice, Product = q};
                }).GroupBy(p => p.categoryPrice,
                    (q, col) => new ResultTask8
                    {
                        categoryPrice = q,
                        Products = col.Select(p => p.Product)
                    });
        }

        /// <summary>
        /// Task9
        /// </summary>
        public IEnumerable<ResultTask9> GetAverageStatisticsForCities()
        {
            return _dataSource.Customers.GroupBy(c => c.City,
                (city, customer) => new ResultTask9
                {
                    City = city,
                    AverageSumPerCustomer = customer.Select(p => p.Orders.Sum(os => os.Total)).Average(),
                    SumByCity = customer.Select(p => p.Orders.Sum(os => os.Total)).Sum(),
                    AverageOrdersSumPerCustomer = customer.Select(p => p.Orders.Count()).Average()
                });
        }
    }

    public class ResultTask4
    {
        public Customer Customer { get; set; }

        public DateTime FirstOrder { get; set; } 
    }

    public class ResultTask7
    {
        public string Key { get; set; }

        public IEnumerable<SubGroup> GroupProducts { get; set; }

        public class SubGroup
        {
            public int Key { get; set; }

            public IEnumerable<Product> Products { get; set; }
        }
    }

    public class ResultTask8
    {
        public string categoryPrice { get; set; }

        public IEnumerable<Product> Products { get; set; }

    }

    public class ResultTask9
    {
        public string City { get; set; }
        public decimal AverageSumPerCustomer { get; set; }
        public decimal SumByCity { get; set; }
        public double AverageOrdersSumPerCustomer { get; set; }
    }
    public class ResultTask10
    {
        public int Month { get; set; }
        public double AverageOrdersPerMonth { get; set; }
    }
}
