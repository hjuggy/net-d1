﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using LinqTask.Data;
using Xunit;

namespace LinqTask.Tests
{
    public class LinqTasksTests
    {
        [Theory]
        [InlineData(1500.00, "Alfreds Futterkiste")]
        public void Should_Be_Return_List_Customers_With_Sum_Totals_More_Param_Threshold(decimal threshold, string companyName)
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetCustomersByAmountMoreThreshold(threshold);
            result.Should().Contain(p => p.CompanyName == companyName);
        }

        [Theory]
        [InlineData("Germany", "Berlin", "Heli Süßwaren GmbH & Co. KG")]
        public void Should_Be_Return_List_Suppliers_In_Th_City_Of_Customer(string country, string city, string supplierName)
        {
            var customer = new Customer
            {
                Country = country,
                City = city
            };
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetSuppliersInCityOfCustomer(customer);
            result.Should().Contain(p => p.SupplierName == supplierName);
        }

        [Theory]
        [InlineData(3192.64, "Berglunds snabbköp")]
        public void Should_Be_Return_List_Customers_With_Order_Total_More_Param_Threshold(decimal threshold, string companyName)
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetCustomersByOrderMoreThreshold(threshold);
            result.Should().Contain(p => p.CompanyName == companyName);
        }

        [Theory]
        [InlineData("1996-11-21T00:00:00", "Seven Seas Imports")]
        public void Should_Be_Contain_companyName_with_DateFirstOrder(string date, string companyName)
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetCustomersWithDateFirstOrder();
            result.Should().Contain(p => p.Customer.CompanyName == companyName && p.FirstOrder == DateTime.Parse(date));
        }

        [Theory]
        [InlineData("1996-12-09T00:00:00", "Seven Seas Imports")]
        public void Should_Be_NotContain_company_with_DateFirstOrder(string date, string companyName)
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetCustomersWithDateFirstOrder();
            result.Should().NotContain(p => p.Customer.CompanyName == companyName && p.FirstOrder == DateTime.Parse(date));
        }

        [Fact]
        public void Should_Be_SortByData()
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetCustomersWithDateFirstOrderSort();
            result.Should().BeInAscendingOrder(p => p.FirstOrder);
        }
        [Theory]
        [InlineData("1996-11-26T00:00:00")]
        public void Should_Be_SortByCompanyName(string data)
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetCustomersWithDateFirstOrderSort()
                .Where(p => p.FirstOrder == DateTime.Parse(data));
            result.Should().BeInDescendingOrder(r => r.Customer.CompanyName);
        }

        [Fact]
        public void Should_Be_Contain_Customers_With_Special_Filling()
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetCustomersWithSpecialFilling();
            result.Should().Contain(p=> !(p.PostalCode is int)
                                    || string.IsNullOrWhiteSpace(p.Region)
                                    || p.Phone[0] != '(');
        }

        [Theory]
        [InlineData("Simons bistro")]
        [InlineData("Spécialités du monde")]
        [InlineData("The Big Cheese")]
        public void Should_Be_Contain_Customers_With_companyName(string companyName)
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetCustomersWithSpecialFilling();
            result.Should().Contain(p => p.CompanyName == companyName);
        }
        
        [Fact]
        public void Should_Be_Grouping_By_Category()
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetProduct();
            result.Should().NotBeNull();
        }

        [Fact]
        public void Should_Be_Grouping_By_Price_Contain_High()
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetProductByPrice();
            result.Should().Contain(p => p.categoryPrice == "High");
        }

        [Fact]
        public void Should_Be_Grouping_By_Price_Count_Tree()
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetProductByPrice();
            result.Should().HaveCount(3);
        }

        [Theory]
        [InlineData("Portland")]
        [InlineData("Kirkland")]
        public void Should_Be_Contain_City(string city)
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetAverageStatisticsForCities();
            result.Should().Contain(p => p.City == city);
        }

        [Theory]
        [InlineData(69)]
        public void Should_Be_Count_more(int count)
        {
            var linqTasks = new LinqTasks();
            var result = linqTasks.GetAverageStatisticsForCities();
            result.Should().HaveCount(count);
        }
    }
}