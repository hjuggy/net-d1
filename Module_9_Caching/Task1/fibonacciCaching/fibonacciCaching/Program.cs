﻿using System;
using System.Threading;

namespace fibonacciCaching
{
    class Program
    {
        static void Main(string[] args)
        {
            long[] arr = { 5, 7, 6, 5 };
            FibonacciCacheFromSystemRuntimeCaching fCFromSystemRuntimeCaching = new FibonacciCacheFromSystemRuntimeCaching();
            foreach (var num in arr)
            {
                Console.WriteLine($"FibonacciNum = {num} \t FibonacciResult = {fCFromSystemRuntimeCaching.GetFibonacci(num)}");
            }

            FibonacciCacheFromRedis fCFromRedis = new FibonacciCacheFromRedis();
            foreach (var num in arr)
            {
                Console.WriteLine($"FibonacciNum = {num} \t FibonacciResult = {fCFromRedis.GetFibonacci(num)}");
            }

            Console.ReadKey();
        }
    }
}
