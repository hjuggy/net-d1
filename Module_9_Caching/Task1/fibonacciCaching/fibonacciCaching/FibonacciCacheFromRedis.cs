﻿using System;
using System.Configuration;
using StackExchange.Redis;

namespace fibonacciCaching
{
    public class FibonacciCacheFromRedis
    {
        private readonly Lazy<ConnectionMultiplexer> LazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            var cacheConnection = ConfigurationManager.AppSettings["CacheConnection"];
            return ConnectionMultiplexer.Connect(cacheConnection);
        });

        public ConnectionMultiplexer Connection => LazyConnection.Value;

        public string GetFibonacci(long num)
        {
            var db = Connection.GetDatabase();

            var cacheItem = db.StringGet(num.ToString());
            if (cacheItem.IsNull)
            {
                var fibonacciNonCache = Fibonacci.Result(num);

                db.StringSet(num.ToString(), fibonacciNonCache);

                return fibonacciNonCache.ToString();
            }

            Console.WriteLine("result from cache");
            return cacheItem;
        }
    }
}