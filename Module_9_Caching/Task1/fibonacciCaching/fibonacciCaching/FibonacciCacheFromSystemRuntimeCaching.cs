﻿using System;
using System.Runtime.Caching;

namespace fibonacciCaching
{
    public class FibonacciCacheFromSystemRuntimeCaching
    {
        public string GetFibonacci(long num)
        {
            using (var cache = MemoryCache.Default)
            {
                CacheItem fibonacci = cache.GetCacheItem(num.ToString());

                if (fibonacci == null)
                {
                    CacheItemPolicy policy = new CacheItemPolicy();

                    var fibonacciNonCache = Fibonacci.Result(num);

                    cache.Set(new CacheItem(num.ToString(), fibonacciNonCache), policy);

                    return fibonacciNonCache.ToString();
                }

                Console.WriteLine("result from cache");
                return fibonacci.Value.ToString();
            }
        }
    }
}
