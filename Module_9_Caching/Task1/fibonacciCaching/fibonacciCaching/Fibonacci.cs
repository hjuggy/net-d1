﻿using System;

namespace fibonacciCaching
{
    public static class Fibonacci
    {
        public static long Result(long num)
        {
            long fibonacciPrevious = 0;
            long fibonacciNext = 1;
            for (int i = 0; i < num; i++)
            {
                long temp = fibonacciPrevious;
                fibonacciPrevious = fibonacciNext;
                fibonacciNext = temp + fibonacciNext;
            }
            Console.WriteLine("counted result");
            return fibonacciPrevious;
        }
    }
}
