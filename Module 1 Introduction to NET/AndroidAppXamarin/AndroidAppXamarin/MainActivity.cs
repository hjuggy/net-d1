﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using LibraryMultitargeting;

namespace AndroidAppXamarin
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            Button btnHello = FindViewById<Button>(Resource.Id.button1);
            btnHello.Click += BtnHello_Click;
        }

        private void BtnHello_Click(object sender, System.EventArgs e)
        {
            EditText txtEdit = FindViewById<EditText>(Resource.Id.editText1);
            TextView txtView = FindViewById<TextView>(Resource.Id.textView1);

            ////task 1
            //txtView.Text = $"Hello {txtEdit.Text}!";

            //task 2
            txtView.Text = Multitargeting.Hello(txtEdit.Text);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}