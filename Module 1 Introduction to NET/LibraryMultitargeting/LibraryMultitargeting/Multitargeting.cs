﻿using System;

namespace LibraryMultitargeting
{
    public class Multitargeting
    {
        public static string Hello(string name)
        {
            return $"{DateTime.Now.ToLongTimeString()} Hello, {name}!";
        }
    }
}
