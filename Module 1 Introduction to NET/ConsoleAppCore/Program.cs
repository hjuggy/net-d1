﻿using System;
using LibraryMultitargeting;

namespace ConsoleAppCore
{
    class Program
    {
        static void Main(string[] args)
        {
            ////task1
            //Console.WriteLine($"Hello, {args[0]}!");

            //task2
            Console.WriteLine(Multitargeting.Hello(args[0]));

            Console.Read();

        }
    }
}
