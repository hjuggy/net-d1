﻿using LibraryMultitargeting;
using System;
using System.Windows.Forms;



namespace WindowsFormsApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void butHello_Click(object sender, EventArgs e)
        {
            ////task1
            //label1.Text = $"Hello {txtName.Text}!";

            //task2
            label1.Text = Multitargeting.Hello(txtName.Text);
        }
    }
}
