﻿using System.Linq;
using System.Runtime.Serialization;
using StyleCop;
using StyleCop.CSharp;

namespace StyleCopCustomRule
{
    public class EntetyRule : SourceAnalyzer
    {
        private readonly string entetyPublicRule = "EntetyPublicRule";
        private readonly string entetyAttributeRule = "EntetyDataContractAttributeRule";
        private readonly string entetyPublicPropIdRule = "EntetyPublicPropIdRule";
        private readonly string entetyPublicPropNameRule = "EntetyPublicPropNameRule";

        public override void AnalyzeDocument(CodeDocument document)
        {
            CsDocument csharpDocument = (CsDocument)document;

            if (csharpDocument.RootElement != null && !csharpDocument.RootElement.Generated)
            {
                csharpDocument.WalkDocument(VisitNamespace);
            }
        }

        private bool VisitNamespace(CsElement element, CsElement parentElement, object context)
        {
            if (element.Name.EndsWith(".Entities", System.StringComparison.Ordinal))
            {
                return VisitClass(element);
            }

            return true;
        }


        private bool VisitClass(CsElement element)
        {
            var chElements = element.ChildElements;

            foreach (var chElement in chElements)
            {
                if (chElement is Class)
                {
                    if (chElement.Declaration.AccessModifierType != AccessModifierType.Public)
                    {
                        AddViolation(chElement, entetyPublicRule);
                    }
                    else
                    {
                        return VisitProps(chElement);
                    }
                }
            }
            return false;
        }

        private bool VisitProps(CsElement element)
        {
            var chElements = element.ChildElements;

            if (chElements.All(p => p.Name != "Id" && p.Declaration.ElementType == ElementType.Property))
            {
                AddViolation(element, entetyPublicPropIdRule);
            }

            if (chElements.All(p => p.Name != "Name" && p.Declaration.ElementType == ElementType.Property))
            {
                AddViolation(element, entetyPublicPropNameRule);
            }

            foreach (var chElement in chElements)
            {
                if (chElement.Declaration.ElementType == ElementType.Property)
                {
                    if (chElement.Attributes.All(att => att.Text != nameof(DataContractAttribute)))
                    {
                        AddViolation(element, entetyAttributeRule);
                    }
                }
            }

            return false;
        }
    }
}