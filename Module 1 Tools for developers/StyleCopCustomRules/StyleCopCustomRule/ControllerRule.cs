﻿using StyleCop;
using StyleCop.CSharp;
using System;
using System.Linq;
using System.Web.Mvc;

namespace StyleCopCustomRule
{
    [SourceAnalyzer(typeof(CsParser))]
    public class ControllerRule : SourceAnalyzer
    {
        private readonly string controllerNameRule = "ControllerNameRule";
        private readonly string controllerClassRule = "ControllerClassAttributeRule";
        private readonly string controllerMethodRule = "ControllerMethodAttributeRule";

        public override void AnalyzeDocument(CodeDocument document)
        {
            CsDocument csharpDocument = (CsDocument)document;

            if (csharpDocument.RootElement != null && !csharpDocument.RootElement.Generated)
            {
                csharpDocument.WalkDocument(VisitElement);
            }
        }

        private bool VisitElement(CsElement element, CsElement parentElement, object context)
        {
            if (element is Class elementClass)
            {
                Type typeElement = Type.GetType(elementClass.Name);

                if (typeElement == typeof(Controller))
                {
                    if (!elementClass.Name.EndsWith("Controller", System.StringComparison.Ordinal))
                    {
                        AddViolation(element, controllerNameRule);
                    }

                    return VisitClass(element);
                }
            }

            return true;
        }

        private bool VisitClass(CsElement element)
        {
            if (element.Attributes.All(att => att.Text != nameof(AuthorizeAttribute)))
            {
                AddViolation(element, controllerClassRule);
            }

            return VisitMethod(element);
        }

        private bool VisitMethod(CsElement element)
        {
            var chElements = element.ChildElements;

            foreach (var chElement in chElements)
            {
                if (chElement.Declaration.AccessModifierType == AccessModifierType.Public)
                {
                    if (element.Attributes.All(att => att.Text != nameof(AuthorizeAttribute)))
                    {
                        AddViolation(element, controllerMethodRule);
                    }
                }
            }
            return false;
        }
    }
}