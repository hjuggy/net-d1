﻿using NUnit.Framework;
using System.Linq;

namespace FileSystemConsoleApp.FileSystemFinding.Tests
{
    [TestFixture]
    public class FileSystemVisitorTests
    {
        //TestFolder is contains 12 items 
        [TestCase(@"D:\Work_Tamash\D1 NET\net-d1\Module 2 Fundamentals\FileSystemConsoleApp\FileSystemConsoleAppTests\TestFolder", 12)]
        public void FileSystemVisitor_Should_Return_TrueResult(string path, int result)
        {
            FileSystemVisitor fs = new FileSystemVisitor
            {
                Path = path
            };
            fs.FindItems();
            Assert.IsTrue(fs.Count() == result);
        }

        //TestFolder is contains 1 item with Length more 30 chars
        [TestCase(@"D:\Work_Tamash\D1 NET\net-d1\Module 2 Fundamentals\FileSystemConsoleApp\FileSystemConsoleAppTests\TestFolder", 1, 30)]
        public void FileSystemVisitor_Should_Return_TrueResult(string path, int result, int highLength)
        {
            FileSystemVisitor fs = new FileSystemVisitor(p => p.Name.Length > highLength)
            {
                Path = path
            };
            fs.FindItems();

            Assert.IsTrue(fs.Count() == result);
        }

        //event "Start Finding" invoke
        [TestCase(@"D:\Work_Tamash\D1 NET\net-d1\Module 2 Fundamentals\FileSystemConsoleApp\FileSystemConsoleAppTests\TestFolder")]
        public void FileSystemVisitor_Should_Invoke_StartFinding(string path)
        {
            FileSystemVisitor fs = new FileSystemVisitor
            {
                Path = path
            };
            bool tmp = false;
            fs.StateFinding += delegate (object sender, StateFindingArgs e)
            {
                if (e.StateEvent == StateFindingEnum.Start)
                {
                    tmp = true;
                }
            };
            fs.FindItems();

            Assert.IsTrue(tmp);
        }

        //event "Finish Finding" invoke
        [TestCase(@"D:\Work_Tamash\D1 NET\net-d1\Module 2 Fundamentals\FileSystemConsoleApp\FileSystemConsoleAppTests\TestFolder")]
        public void FileSystemVisitor_Should_Invoke_FinishFinding(string path)
        {
            FileSystemVisitor fs = new FileSystemVisitor
            {
                Path = path
            };
            bool tmp = false;
            fs.StateFinding += delegate (object sender, StateFindingArgs e)
            {
                if (e.StateEvent == StateFindingEnum.Finish)
                {
                    tmp = true;
                }
            };
            fs.FindItems();

            Assert.IsTrue(tmp);
        }

        //events "FileFinded" invoke
        [TestCase(@"D:\Work_Tamash\D1 NET\net-d1\Module 2 Fundamentals\FileSystemConsoleApp\FileSystemConsoleAppTests\TestFolder")]
        public void FileSystemVisitor_Should_Invoke_FileFinded(string path)
        {
            FileSystemVisitor fs = new FileSystemVisitor
            {
                Path = path
            };
            bool tmp = false;
            fs.StateItemFinding += delegate (object sender, StateItemFindingArgs e)
            {
                if (e.StateEvent == StateFindingEnum.FileFinded)
                {
                    tmp = true;
                }
            };
            fs.FindItems();

            Assert.IsTrue(tmp);
        }

        //events "DirectoryFinded" invoke
        [TestCase(@"D:\Work_Tamash\D1 NET\net-d1\Module 2 Fundamentals\FileSystemConsoleApp\FileSystemConsoleAppTests\TestFolder")]
        public void FileSystemVisitor_Should_Invoke_DirectoryFinded(string path)
        {
            FileSystemVisitor fs = new FileSystemVisitor
            {
                Path = path
            };
            bool tmp = false;
            fs.StateItemFinding += delegate (object sender, StateItemFindingArgs e)
            {
                if (e.StateEvent == StateFindingEnum.DirectoryFinded)
                {
                    tmp = true;
                }
            };
            fs.FindItems();

            Assert.IsTrue(tmp);
        }

        //events "FilteredFileFinded" invoke (file with a name longer than 30 characters none)
        [TestCase(@"D:\Work_Tamash\D1 NET\net-d1\Module 2 Fundamentals\FileSystemConsoleApp\FileSystemConsoleAppTests\TestFolder", 30)]
        public void FileSystemVisitor_Should_Invoke_FilteredFileFinded(string path, int highLength)
        {
            FileSystemVisitor fs = new FileSystemVisitor(p => p.Name.Length > highLength)
            {
                Path = path
            };
            bool tmp = false;
            fs.StateItemFinding += delegate (object sender, StateItemFindingArgs e)
            {
                if (e.StateEvent == StateFindingEnum.FilteredFileFinded)
                {
                    tmp = true;
                }
            };
            fs.FindItems();

            Assert.IsFalse(tmp);
        }

        //events "FilteredDirectoryFinded" invoke (Directory with a name longer than 30 characters exists)
        [TestCase(@"D:\Work_Tamash\D1 NET\net-d1\Module 2 Fundamentals\FileSystemConsoleApp\FileSystemConsoleAppTests\TestFolder", 30)]
        public void FileSystemVisitor_Should_Invoke_FilteredDirectoryFinded(string path, int highLength)
        {
            FileSystemVisitor fs = new FileSystemVisitor(p => p.Name.Length > highLength)
            {
                Path = path
            };
            bool tmp = false;
            fs.StateItemFinding += delegate (object sender, StateItemFindingArgs e)
            {
                if (e.StateEvent == StateFindingEnum.FilteredDirectoryFinded)
                {
                    tmp = true;
                }
            };
            fs.FindItems();

            Assert.IsTrue(tmp);
        }
    }
}