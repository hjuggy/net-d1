﻿using System;
using System.IO;
using System.Threading;
using FileSystemConsoleApp.FileSystemFinding;

namespace FileSystemConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            FileSystemVisitor fs = new FileSystemVisitor();

            fs.StateFinding += Fs_StateFinding;
            fs.StateItemFinding += Fs_StateItemFinding;
            Console.WriteLine("Input path");
            fs.Path = Console.ReadLine();
            fs.FindItems();
            Console.Read();
        }

        private static void Fs_StateItemFinding(object sender, StateItemFindingArgs e)
        {
            switch (e.StateEvent)
            {
                case StateFindingEnum.FileFinded:
                    Console.WriteLine($"File {e.CurrentItem.Name} was found at {e.TimeEvent.ToLongTimeString()}");
                    StoppingSkippingFinding(e);
                    break;
                case StateFindingEnum.FilteredFileFinded:
                    Console.WriteLine($"File {e.CurrentItem.Name} was found at {e.TimeEvent.ToLongTimeString()}");
                    StoppingSkippingFinding(e);
                    break;
                case StateFindingEnum.DirectoryFinded:
                    Console.WriteLine($"Directory {e.CurrentItem.Name} was found at {e.TimeEvent.ToLongTimeString()}");
                    StoppingSkippingFinding(e);
                    break;
                case StateFindingEnum.FilteredDirectoryFinded:
                    Console.WriteLine($"Directory {e.CurrentItem.Name} was found at {e.TimeEvent.ToLongTimeString()}");
                    StoppingSkippingFinding(e);
                    break;
            }
        }

        private static void StoppingSkippingFinding(StateItemFindingArgs e)
        {
            if (e.CurrentItem.Name.Length>20)
            {
                e.SkipItem = true;
            }

            if (e.CurrentItem.Name.Contains("Tank"))
            {
                e.BreakFinding = true;
            }
        }

        private static void Fs_StateFinding(object sender, StateFindingArgs e)
        {
            if (e.StateEvent==StateFindingEnum.Start)
            {
                Console.WriteLine($"Finding is run at {e.TimeEvent}");
            }
            else if (e.StateEvent == StateFindingEnum.Finish)
            {
                Console.WriteLine($"Finding is end  at {e.TimeEvent}");
            }
        }
    }
}
