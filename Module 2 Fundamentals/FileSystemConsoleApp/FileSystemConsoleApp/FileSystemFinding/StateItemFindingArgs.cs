﻿using System;

namespace FileSystemConsoleApp.FileSystemFinding
{
    public class StateItemFindingArgs
    {
        public StateFindingEnum StateEvent { get; set; }
        public Item CurrentItem { get; set; }
        public DateTime TimeEvent { get; set; }
        public bool SkipItem { get; set; } = false;
        public bool BreakFinding { get; set; } = false;
    }
}