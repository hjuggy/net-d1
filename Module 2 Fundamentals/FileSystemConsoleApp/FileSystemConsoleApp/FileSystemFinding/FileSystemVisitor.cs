﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileSystemConsoleApp.FileSystemFinding
{
    public class FileSystemVisitor : IEnumerable<Item>
    {
        private string _dir;

        private readonly List<Item> _items = new List<Item>();
        
        private readonly Func<FileSystemInfo,bool> _filterPredicate;

        public event EventHandler<StateItemFindingArgs> StateItemFinding;

        public event EventHandler<StateFindingArgs> StateFinding;

        StateFindingArgs _stateFindingArgs = new StateFindingArgs();

        StateItemFindingArgs _stateItemFindingArgs = new StateItemFindingArgs();
        
        public string Path
        {
            get => _dir;
            set
            {
                if (Directory.Exists(value))
                {
                    _dir = value;
                }
                else
                {
                    throw new ArgumentException($"Path {value} does not exist");
                }
            }
        }

        public FileSystemVisitor() { }
        public FileSystemVisitor(Func<FileSystemInfo, bool> filterPredicate) => _filterPredicate = filterPredicate;

        protected virtual void OnStateItemFinding(StateItemFindingArgs args, Item currentItem, StateFindingEnum stateFindingEnum)
        {
            args.StateEvent = stateFindingEnum;
            args.CurrentItem = currentItem;
            args.TimeEvent = DateTime.Now;
            var tmpEvent = StateItemFinding;

            tmpEvent?.Invoke(this, args);
        }

        protected virtual void OnStateFinding(StateFindingArgs args, StateFindingEnum stateFindingEnum)
        {
            args.StateEvent = stateFindingEnum;
            args.TimeEvent= DateTime.Now;
            var tmpEvent = StateFinding;

            tmpEvent?.Invoke(this, args);
        }

        public void FindItems()
        {
            if (!string.IsNullOrEmpty(Path) || Directory.Exists(Path))
            {

                OnStateFinding(_stateFindingArgs,  StateFindingEnum.Start);

                DirectoryInfo directory = new DirectoryInfo(Path);

                IEnumerable<FileSystemInfo> fileSystemInfos;
                Item tmpItem;

                if (_filterPredicate!=null)
                { 
                    fileSystemInfos = directory.EnumerateFileSystemInfos("*", SearchOption.AllDirectories).Where(_filterPredicate);
                }
                else
                {
                    fileSystemInfos = directory.EnumerateFileSystemInfos("*", SearchOption.AllDirectories);
                }

                foreach (var fileSystemInfo in fileSystemInfos)
                {
                    if (fileSystemInfo is DirectoryInfo folder)
                    {
                        tmpItem = new Item
                        {
                            Name = folder.Name,
                            FullName = folder.FullName,
                            TypeItem = TypeItemEnum.Folder,
                            Children = GetChildList(folder),
                        };

                        OnStateItemFinding(
                            _stateItemFindingArgs,
                            tmpItem,
                            _filterPredicate == null ? StateFindingEnum.DirectoryFinded : StateFindingEnum.FilteredDirectoryFinded
                        );
                        if (_stateItemFindingArgs.BreakFinding)
                        {
                            break;
                        }
                        if (!_stateItemFindingArgs.SkipItem)
                        {
                            _items.Add(tmpItem);
                        }
                        else
                        {
                            _stateItemFindingArgs.SkipItem = false;
                        }
                    }

                    if (fileSystemInfo is FileInfo file)
                    {
                        tmpItem = new Item
                        {
                            Name = file.Name,
                            FullName = file.FullName,
                            TypeItem = TypeItemEnum.File,
                            Children = null,
                        };

                        OnStateItemFinding(
                            _stateItemFindingArgs,
                            tmpItem,
                            _filterPredicate == null ? StateFindingEnum.FileFinded : StateFindingEnum.FilteredFileFinded
                        );
                        if (_stateItemFindingArgs.BreakFinding)
                        {
                            break;
                        }
                        if (!_stateItemFindingArgs.SkipItem)
                        {
                            _items.Add(tmpItem);
                        }
                        else
                        {
                            _stateItemFindingArgs.SkipItem = false;
                        }
                    }
                }

                OnStateFinding(_stateFindingArgs, StateFindingEnum.Finish);
            }
            else
            {
                throw new ArgumentException($"Path {Path} does not exist or empty");
            }
        }

        private List<string> GetChildList(DirectoryInfo folder)
        {
            List<string> children = new List<string>();
            if (folder != null)
            {
                foreach (var childFolder in folder.EnumerateDirectories())
                {
                    children.Add(childFolder.FullName);
                }
                foreach (var childFile in folder.EnumerateFiles())
                {
                    children.Add(childFile.FullName);
                }
            }

            return children;
        }

        public IEnumerator<Item> GetEnumerator()
        {
            foreach (var item in _items)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
