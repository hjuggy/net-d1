﻿namespace FileSystemConsoleApp.FileSystemFinding
{
    public enum StateFindingEnum
    {
        Start,
        Finish,
        FileFinded,
        DirectoryFinded,
        FilteredFileFinded,
        FilteredDirectoryFinded,

    }
}