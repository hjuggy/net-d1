﻿using System;

namespace FileSystemConsoleApp.FileSystemFinding
{
    public class StateFindingArgs
    {
        public StateFindingEnum StateEvent { get; set; }
        public DateTime TimeEvent { get; set; }
    }
}