﻿using System.Collections.Generic;

namespace FileSystemConsoleApp.FileSystemFinding
{
    public struct Item
    {
        public string Name { get; set; }
        public string FullName { get; set; }
        public TypeItemEnum TypeItem { get; set; }
        public List<string> Children { get; set; } 
    }
}