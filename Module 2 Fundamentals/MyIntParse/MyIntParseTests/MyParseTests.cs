﻿using NUnit.Framework;
using System;

namespace MyIntParse.Tests
{
    [TestFixture]
    public class MyParseTests
    {
        [TestCase("0",0)]
        [TestCase("36544464", 36544464)]
        [TestCase("-5544", -5544)]
        public void IntMyParse_Should_Return_True(string arg, int result)
        {
            var res = MyParse.IntMyParse(arg);
            Assert.IsTrue(res==result);
        }

        [TestCase("554454455440")]
        [TestCase("-554454455440")]
        public void IntMyParse_Should_Throws_ArgumentOutOfRangeException(string arg)
        {
            Assert.Throws<ArgumentOutOfRangeException>(()=>MyParse.IntMyParse(arg));
        }

        [TestCase("5fgd454")]
        [TestCase("-555hg440")]
        [TestCase("45 34")]
        public void IntMyParse_Should_Throws_ArgumentException(string arg)
        {
            Assert.Throws<ArgumentException>(() => MyParse.IntMyParse(arg));
        }
    }
}