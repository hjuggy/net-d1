﻿using System;

namespace MyIntParse
{
    public static class MyParse
    {
        public static int IntMyParse(string str)
        {
            char[] chs = str.ToCharArray();

            int sign = 1;
            if (chs[0] == '-')
            {
                sign = -1;
                chs = str.ToCharArray(1, chs.Length - 1);
            }

            long result = 0;
            long tenPower = 1;
            for (int i = chs.Length - 1; i > -1; i--)
            {
                long temp = chs[i];
                if (temp < 48 || temp > 57)
                {
                    throw new ArgumentException(message: "Can not be converted to a number");
                }
                result += (temp - 48) * tenPower;
                tenPower *= 10;
            }

            result = result * sign;
            if (result > int.MaxValue || result < int.MinValue)
            {
                throw new ArgumentOutOfRangeException();
            }

            return (int)result;
        }
    }
}
