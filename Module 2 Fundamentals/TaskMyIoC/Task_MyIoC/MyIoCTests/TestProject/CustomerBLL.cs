﻿namespace MyIoC
{
    [ImportConstructor]
    public class CustomerBLL
    {
        public ICustomerDAL _dal { get; set; }
        public Logger _logger { get; set; }
        public CustomerBLL(ICustomerDAL dal, Logger logger)
        {
            _dal = dal;
            _logger = logger;
        }
    }
}