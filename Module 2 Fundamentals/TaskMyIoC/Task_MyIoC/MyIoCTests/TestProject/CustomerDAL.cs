﻿namespace MyIoC
{
    [Export(typeof(ICustomerDAL))]
    public class CustomerDAL : ICustomerDAL
    {
        public string GetMessage()
        {
            return "Hello Inject"; 
        }
    }
}