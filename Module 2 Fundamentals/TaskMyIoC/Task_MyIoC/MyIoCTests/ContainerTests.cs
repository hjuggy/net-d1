﻿using FluentAssertions;
using System.Reflection;
using Xunit;

namespace MyIoC.Tests
{
    public class ContainerTests
    {

        [Fact]
        public void ContainerCtorIsValid()
        {
            var container = new Container();
            container.Should().BeOfType<Container>();
        }

        [Fact]
        public void AddAssemblyMethod_Should_Add_CustomerDAL_From_AssemblyTestProject()
        {
            var container = new Container();
            container.AddAssembly(Assembly.GetExecutingAssembly());
            var result = container.CreateInstance<CustomerDAL>();
            result.Should().BeOfType<CustomerDAL>();
        }

        [Fact]
        public void AddType_Should__Add_CustomerDAL()
        {
            var container = new Container();
            container.AddType(typeof(CustomerDAL));
            var result = container.CreateInstance<CustomerDAL>();
            result.Should().BeOfType<CustomerDAL>();
        }

        [Fact]
        public void AddType_Should_Add_CustomerDAL_As_ICustomerDAL()
        {
            var container = new Container();
            container.AddType(typeof(CustomerDAL), typeof(ICustomerDAL));
            var result = container.CreateInstance<ICustomerDAL>();
            result.Should().BeOfType<CustomerDAL>();
        }

        [Fact]
        public void CreateInstance_Should_Return_CustomerDAL_for_ICustomerDAL()
        {
            var container = new Container();
            container.AddType(typeof(CustomerDAL), typeof(ICustomerDAL));
            var result = container.CreateInstance<ICustomerDAL>();
            result.Should().BeOfType<CustomerDAL>();
        }

        [Fact]
        public void CreateInstance_Should_Return_Object_Cast_CustomerDAL()
        {
            var container = new Container();
            container.AddType(typeof(CustomerDAL), typeof(ICustomerDAL));
            var result = container.CreateInstance(typeof(ICustomerDAL));
            result.Should().BeOfType<CustomerDAL>();
        }

        [Fact]
        public void Build_Should_Injected_CustomerDAL_To_CustomerBLL_By_Attribute()
        {
            var container = new Container();
            container.AddAssembly(Assembly.GetExecutingAssembly());
            container.AddType(typeof(Logger));
            container.AddType(typeof(CustomerDAL), typeof(ICustomerDAL));
            container.AddType(typeof(CustomerBLL));
            container.Build();
            var result = container.CreateInstance<CustomerBLL>();
            result._dal.GetMessage().Should().Contain("Hello Inject");
        }
    }
}