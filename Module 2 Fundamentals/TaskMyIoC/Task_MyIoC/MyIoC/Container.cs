﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MyIoC
{
    public class Container
    {
        private Dictionary<Type, Description> _mapContainer;

        Assembly _assembly;

        public Container()
        {
            _mapContainer = new Dictionary<Type, Description>();
        }

        public void AddAssembly(Assembly assembly)
        {
            _assembly = Assembly.LoadFrom(assembly.Location);

            var typesWithExportAttribute = _assembly.GetTypes().Where(p => p.GetCustomAttribute<ExportAttribute>() != null).ToList();

            foreach (var typeWithExportAttribute in typesWithExportAttribute)
            {
                var att = typeWithExportAttribute.GetCustomAttribute<ExportAttribute>();
                if (att.Contract != null)
                {
                    _mapContainer[typeWithExportAttribute] = new Description
                    {
                        ImplementType = att.Contract,
                        ResolveType = typeWithExportAttribute,
                    };
                }
                else
                {
                    AddType(typeWithExportAttribute);
                }
            }
        }

        public void AddType(Type type)
        {
            if (type.IsClass)
            {
                _mapContainer[type] = new Description
                {
                    ImplementType = type,
                    ResolveType = type,
                };
            }
        }

        public void AddType(Type type, Type baseType)
        {
            _mapContainer[baseType] = new Description
            {
                ImplementType = baseType,
                ResolveType = type,
            };
        }

        public object CreateInstance(Type type)
        {
            if (_mapContainer.TryGetValue(type, out var resultDescription))
            {
                var resolveType = resultDescription.ResolveType;

                var resolveObj = InjectToCtor(resolveType);

                InjectToProperty(resolveType, resolveObj);
                return resolveObj;
            }

            throw new Exception($"We don't resolve type {nameof(type)}");
        }

        public T CreateInstance<T>() where T : class
        {
            var type = typeof(T);
            return (T)CreateInstance(type);
        }

        public void Build()
        {
            var typesWithImportConstructorAttribute = _assembly.GetTypes().Where(p => p.GetCustomAttribute<ImportConstructorAttribute>() != null).ToList();

            foreach (var typeWithImportConstructorAttribute in typesWithImportConstructorAttribute)
            {
                InjectToCtor(typeWithImportConstructorAttribute);
            }

            var types = _assembly.GetTypes().Where(p => p.GetCustomAttribute<ImportAttribute>() != null);


            foreach (var type in types)
            {
                if (_mapContainer.TryGetValue(type, out var resultDescription))
                {
                    var resolveType = resultDescription.ResolveType;

                    var resolveObj = InjectToCtor(resolveType);
                    InjectToProperty(resolveType, resolveObj);
                }
            }
        }

        private object InjectToCtor(Type resolveType)
        {
            object[] ctorParams = null;
            var ctors = GetConstructorsInfo(resolveType);
            var ctor = ctors.FirstOrDefault(ct => ResolveDependency(ct, out ctorParams));

            return ctor?.Invoke(ctorParams);
        }

        private void InjectToProperty(Type resolveType, object resolveObj)
        {
            var props = resolveType.GetProperties();
            var injectingProperties = props.Where(pr => pr.GetCustomAttribute<ImportAttribute>() != null);

            foreach (var prop in injectingProperties)
            {
                prop.SetValue(resolveObj, CreateInstance(prop.PropertyType));
            }
        }

        private ICollection<ConstructorInfo> GetConstructorsInfo(Type type)
        {
            var constructorInfos = type.GetConstructors();
            var sorted = constructorInfos.OrderBy(x => x.GetParameters().Length).ToList();
            return sorted;
        }

        private bool ResolveDependency(ConstructorInfo ctorInfo, out object[] ctorParams)
        {
            var ctorArgs = ctorInfo.GetParameters();
            ctorParams = new object[ctorArgs.Length];
            for (int i = 0; i < ctorParams.Length; ++i)
            {
                try
                {
                    ctorParams[i] = CreateInstance(ctorArgs[i].ParameterType);
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return true;
        }

        private class Description
        {
            public Type ImplementType { get; set; }

            public Type ResolveType { get; set; }
        }
    }
}