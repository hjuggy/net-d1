﻿using System;

namespace MyIoC
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class ImportAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Class)]
	public class ImportConstructorAttribute : Attribute
	{
	}

    [Export]
	public class ContractBLL {}

	[Export]
	public class ContractDLL { }

}
