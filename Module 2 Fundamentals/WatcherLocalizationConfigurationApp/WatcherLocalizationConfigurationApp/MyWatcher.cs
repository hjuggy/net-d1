﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WatcherLocalizationConfigurationApp.Configuration;

namespace WatcherLocalizationConfigurationApp
{
    public class MyWatcher
    {

        MovingRulesElement[] _movingRules;
        public MyWatcher(MovingRulesElement[] movingRules)
        {
            _movingRules = movingRules;
        }

        public void RunWatch(FolderElement[] folderElements)
        {

            foreach (FolderElement folder in folderElements)
            {
                Watch(folder.Path);
            }
            
            Console.WriteLine(Resources.quit);
            while (Console.Read() != 'q');
        }
        private void Watch(string watch_folder)
        {

            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = watch_folder;

            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            watcher.Filter = "*.txt";

            watcher.Created += new FileSystemEventHandler(OnChanged);

            watcher.EnableRaisingEvents = true;
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            for (int i = 0; i < _movingRules.Length; i++)
            {
                MovingRulesElement movingRule = _movingRules[i];
                if (Regex.IsMatch(e.Name, movingRule.Rule, RegexOptions.None ))
                {
                    Moving(e, movingRule);
                    break;
                }
            }

            Moving(e, null);

        }

        private void Moving(FileSystemEventArgs e, MovingRulesElement movingRule)
        {
            if (movingRule !=null)
            {
                File.Move(e.FullPath, movingRule.PathMoving);
                Console.WriteLine($"{e.FullPath} {Resources.moved} {movingRule.PathMoving}.");
            }
            else
            {
                var pathAll = @"D:\Watch\All";
                DirectoryInfo directory = new DirectoryInfo(e.FullPath);
                directory.MoveTo(pathAll);
                Console.WriteLine($"{e.FullPath} {Resources.moved} {pathAll}.");
            }

        }
    }
}

