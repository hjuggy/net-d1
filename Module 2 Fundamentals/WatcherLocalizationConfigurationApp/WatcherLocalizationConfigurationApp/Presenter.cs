﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WatcherLocalizationConfigurationApp.Configuration;

namespace WatcherLocalizationConfigurationApp
{
    public class Presenter
    {
        private System.Configuration.Configuration _cfg;
        private AppConfigurationSection _appSection;

        public Presenter(System.Configuration.Configuration cfg, AppConfigurationSection appSection)
        {
            _cfg = cfg;
            _appSection = appSection;
        }

        public void Start()
        {
            Console.WriteLine(Resources.localeSelection);

            foreach (LocationElement location in _appSection.Locations)
            {
                Console.WriteLine($">> {location.sityName}");
            }
            var selectedLocation = Console.ReadLine();

            if (selectedLocation != null && _appSection.Locations.ToDictionary().ContainsKey(selectedLocation))
            {
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(_appSection.Locations.ToDictionary()[selectedLocation]);
            }
            else
            {
                Console.WriteLine(Resources.ErrLocaleSelection);
                Start();
            }
            DescriptionOfWork();
        }

        private void DescriptionOfWork()
        {
            var folders = _appSection.Folders.GetAllKeys();
            string description1 = $"{Resources.Descrip1}: {string.Join(", ", folders)} ";
            Console.WriteLine(description1);
            Console.WriteLine(Resources.Descrip2);
            MovingRulesElement[] movingRules = new MovingRulesElement[_appSection.MovingRules.Count];
            _appSection.MovingRules.CopyTo(movingRules, 0);
            foreach (var movingRule in movingRules)
            {
                Console.WriteLine($"{Resources.Descrip3}: {movingRule.RuleName} {Resources.Descrip3move} {movingRule.FolderName}");
            }
        }
    }
}
