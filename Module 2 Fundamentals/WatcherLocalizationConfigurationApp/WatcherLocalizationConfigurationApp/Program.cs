﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using WatcherLocalizationConfigurationApp.Configuration;

namespace WatcherLocalizationConfigurationApp
{
    class Program
    {
        static void Main(string[] args)
        {       
            System.Configuration.Configuration cfg = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            AppConfigurationSection s = (AppConfigurationSection)cfg.GetSection("appSection");

            MovingRulesElement[] movingRules = new MovingRulesElement[s.MovingRules.Count];
            s.MovingRules.CopyTo(movingRules, 0);

            FolderElement[] folderElements = new FolderElement[s.Folders.Count];
            s.Folders.CopyTo(folderElements, 0);

            Presenter presenter = new Presenter(cfg, s);
            presenter.Start();
            MyWatcher myWatcher = new MyWatcher(movingRules);
            myWatcher.RunWatch(folderElements);

            Console.ReadKey();
        }
    }
}
