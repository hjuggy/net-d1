﻿using System.Configuration;

namespace WatcherLocalizationConfigurationApp.Configuration
{
    public class FoldersElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new FolderElement();
        }

        public object[] GetAllKeys()
        {
            return this.BaseGetAllKeys();
        }


        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FolderElement)element).FolderName;
        }
    }
}
