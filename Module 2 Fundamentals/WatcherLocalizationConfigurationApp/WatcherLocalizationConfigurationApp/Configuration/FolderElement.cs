﻿using System.Configuration;

namespace WatcherLocalizationConfigurationApp.Configuration
{
    public class FolderElement : ConfigurationElement
    {
        [ConfigurationProperty("folder" , IsKey = true)]
        public string FolderName
        {
            get { return (string)base["folder"]; }
        }

        [ConfigurationProperty("path")]
        public string Path
        {
            get { return (string)base["path"]; }
            set { this["path"] = value; }
        }
    }
}