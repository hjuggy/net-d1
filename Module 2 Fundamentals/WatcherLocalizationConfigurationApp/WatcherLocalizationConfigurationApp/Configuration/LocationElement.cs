﻿using System.Configuration;

namespace WatcherLocalizationConfigurationApp.Configuration
{
    public class LocationElement : ConfigurationElement
    {
        [ConfigurationProperty("sity", IsKey = true, IsRequired = true)]
        public string sityName
        {
            get { return (string)base["sity"]; }
            set { this["sity"] = value; }
        }

        [ConfigurationProperty("culture", IsRequired = true)]
        public string cultureName
        {
            get { return (string)base["culture"]; }   
            set { this["culture"] = value; }
        }
    }
}