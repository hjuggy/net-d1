﻿using System.Configuration;

namespace WatcherLocalizationConfigurationApp.Configuration
{
    public class MovingRulesElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new MovingRulesElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((MovingRulesElement)element).RuleName;
        }
    }
}
