﻿using System.Configuration;

namespace WatcherLocalizationConfigurationApp.Configuration
{
    public class MovingRulesElement : ConfigurationElement
    {
        [ConfigurationProperty("ruleName", IsKey = true)]
        public string RuleName
        {
            get { return (string)base["ruleName"]; }
        }

        [ConfigurationProperty("path")]
        public string PathMoving
        {
            get { return (string)base["path"]; }
            set { this["path"] = value; }
        }

        [ConfigurationProperty("rule")]
        public string Rule
        {
            get { return (string)base["rule"]; }
            set { this["rule"] = value; }
        }

        [ConfigurationProperty("folder")]
        public string FolderName
        {
            get { return (string)base["folder"]; }
            set { this["folder"] = value; }
        }
    }
}