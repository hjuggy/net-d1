﻿using System.Configuration;

namespace WatcherLocalizationConfigurationApp.Configuration
{
    public class AppConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("appName")]
        public string ApplicationName
        {
            get { return (string)base["appName"]; }
        }

        [ConfigurationCollection(typeof(LocationElement),
            AddItemName = "location",
            ClearItemsName = "clear",
            RemoveItemName = "del")]
        [ConfigurationProperty("locations")]
        public LocationElementCollection Locations
        {
            get { return (LocationElementCollection)this["locations"]; }
            set { this["locations"] = value; }
        }

        [ConfigurationCollection(typeof(FolderElement),
            AddItemName = "folder",
            ClearItemsName = "clear",
            RemoveItemName = "del")]
        [ConfigurationProperty("folders")]
        public FoldersElementCollection Folders
        {
            get { return (FoldersElementCollection)this["folders"]; }
            set { this["folders"] = value; }
        }

        [ConfigurationCollection(typeof(MovingRulesElement),
            AddItemName = "movingRule",
            ClearItemsName = "clear",
            RemoveItemName = "del")]
        [ConfigurationProperty("movingRules")]
        public MovingRulesElementCollection MovingRules
        {
            get { return (MovingRulesElementCollection)this["movingRules"]; }
            set { this["movingRules"] = value; }
        }
    }
}