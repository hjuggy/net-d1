﻿using System.Collections.Generic;
using System.Configuration;

namespace WatcherLocalizationConfigurationApp.Configuration
{
    public class LocationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new LocationElement();
        }
        public void Add(ConfigurationElement element)
        {
            base.BaseAdd(element);
        }
        public Dictionary<string,string> ToDictionary()
        {
            Dictionary<string, string> temp = new Dictionary<string, string>();
            foreach (LocationElement item in this)
            {
                temp[item.sityName] = item.cultureName;
            }
            return temp;
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((LocationElement)element).sityName;
        }
    }
}
