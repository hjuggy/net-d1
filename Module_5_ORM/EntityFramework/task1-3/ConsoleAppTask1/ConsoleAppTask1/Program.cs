﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTask1
{
    class Program
    {
        static void Main(string[] args)
        {
            string categoryName = "Produce";
            WorkWithContextDBByModel  workWithContextDbByModel= new WorkWithContextDBByModel();
            var produces = workWithContextDbByModel.GetOrderByCategory(categoryName);

            foreach (var produce in produces ?? produces)
            {
                Console.WriteLine($"Number order = {produce.OrderId} \t Customer Name = {produce.CustomerName} \t Product Name = {produce.ProductName}");
            }

            Console.ReadKey();
        }
    }
}
