﻿using System.Collections.Generic;
using System.Linq;
using ConsoleAppTask1.Model;


namespace ConsoleAppTask1
{
    public class WorkWithContextDBByModel
    {
        public IList<MyListOderByCategory> GetOrderByCategory(string categoryName)
        {
            IList<MyListOderByCategory> orders;
            using (var context = new NorthwindByModelDb())
            {
                orders = context.Orders.SelectMany(
                    o=>o.Order_Details.Where(p=>p.Product.Category.CategoryName == categoryName),
                    (o, d)=> new MyListOderByCategory
                    {
                        OrderId=o.OrderID,
                        CustomerName=o.Customer.CompanyName,
                        ProductName=d.Product.ProductName,
                        ListDetails=o.Order_Details
                    }).ToList();
            }
            return orders;
        }
    }

    public class MyListOderByCategory
    {
        public int OrderId { get; set; }
        public string CustomerName { get; set; }
        public string ProductName { get; set; }
        public IEnumerable<Order_Detail> ListDetails { get; set; }
    }
}
