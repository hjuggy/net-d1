﻿namespace ConsoleAppTask1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class V_1_3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Regions", newName: "Regionss");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Regionss", newName: "Regions");
        }
    }
}
