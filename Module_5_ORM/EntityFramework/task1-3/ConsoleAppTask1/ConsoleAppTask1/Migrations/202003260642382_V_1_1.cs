﻿namespace ConsoleAppTask1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class V_1_1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CreditCard",
                c => new
                    {
                        CardID = c.Int(nullable: false, identity: true),
                        Number = c.String(nullable: false, maxLength: 16),
                        ExpirateDate = c.DateTime(nullable: false),
                        CardHolder = c.String(nullable: false, maxLength: 60),
                        EmployeeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CardID)
                .ForeignKey("dbo.Employees", t => t.EmployeeID, cascadeDelete: true)
                .Index(t => t.EmployeeID);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CreditCard", "EmployeeID", "dbo.Employees");
            DropIndex("dbo.CreditCard", new[] { "EmployeeID" });
            DropTable("dbo.CreditCard");
        }
    }
}
