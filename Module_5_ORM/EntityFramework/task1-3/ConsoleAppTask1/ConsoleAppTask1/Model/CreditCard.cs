using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsoleAppTask1.Model
{
    [Table("CreditCard")]
    public class CreditCard
    {
        [Key]
        public int CardID { get; set; }

        [Required]
        [StringLength(16)]
        public string Number { get; set; }

        public DateTime ExpirateDate { get; set; }

        [Required]
        [StringLength(60)]
        public string CardHolder { get; set; }

        public int EmployeeID { get; set; }

        public virtual Employee Employee { get; set; }
    }
}