﻿using System;
using System.Diagnostics;
using System.Security;

namespace MyCounters
{
    public class MyCounters
    {
        public void CreateCategory()
        {
            if (!PerformanceCounterCategory.Exists(PropsMyCounters.CategoryName))
            {
                var counterCreationDataCollection = new CounterCreationDataCollection();

                var logInCounter = new CounterCreationData
                {
                    CounterName = PropsMyCounters.LogInCounter,
                    CounterType = PerformanceCounterType.NumberOfItems32,
                };
                counterCreationDataCollection.Add(logInCounter);

                var logOffCounter = new CounterCreationData
                {
                    CounterName = PropsMyCounters.LogOffCounter,
                    CounterType = PerformanceCounterType.NumberOfItems32
                };
                counterCreationDataCollection.Add(logOffCounter);

                var registerSuccessCounter = new CounterCreationData
                {
                    CounterName = PropsMyCounters.RegisterSuccessCounter,
                    CounterType = PerformanceCounterType.NumberOfItems32
                };
                counterCreationDataCollection.Add(registerSuccessCounter);

                try
                {
                    PerformanceCounterCategory.Create(
                        PropsMyCounters.CategoryName,
                        PropsMyCounters.CategoryHelp,
                        PerformanceCounterCategoryType.MultiInstance,
                        counterCreationDataCollection);
                }
                catch (SecurityException)
                {
                    Console.WriteLine("Do not have permissions");
                }

                Console.WriteLine("Category created");
            }
        }
        public void DeleteCategory()
        {
            if (PerformanceCounterCategory.Exists(PropsMyCounters.CategoryName))
            {
                try
                {
                    PerformanceCounterCategory.Delete(PropsMyCounters.CategoryName);
                }
                catch (SecurityException)
                {
                    Console.WriteLine("Do not have permissions");
                }

                Console.WriteLine("Category deleted");
            }
        }
    }
}