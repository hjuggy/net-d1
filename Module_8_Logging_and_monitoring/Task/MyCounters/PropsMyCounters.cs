﻿namespace MyCounters
{
    public static class PropsMyCounters
    {
        public const string CategoryName = "Category my counters";
        public const string CategoryHelp = "";
        public const string LogInCounter = "logIn Counter";
        public const string LogOffCounter = "logOff Counter";
        public const string RegisterSuccessCounter = "Register Success";
    }
}