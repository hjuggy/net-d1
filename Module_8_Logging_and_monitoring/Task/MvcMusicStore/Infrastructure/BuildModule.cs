﻿using Autofac;
using NLog;

namespace MvcMusicStore.Infrastructure
{
    public class BuildModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(f=> LogManager.GetCurrentClassLogger()).As<ILogger>();
        }
    }
}