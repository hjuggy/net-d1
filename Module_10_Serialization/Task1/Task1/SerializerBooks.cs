﻿using System.IO;
using System.Xml.Serialization;
using Task1.Models;

namespace Task1
{
    public class SerializerBooks
    {
        public Catalog Read(string filePath)
        {
            Catalog catalog;
            var serializer = new XmlSerializer(typeof(Catalog));
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                catalog = serializer.Deserialize(stream) as Catalog;
                stream.Close();
            }

            return catalog;
        }

        public void Write(Catalog catalog, string filePath)
        {
            var serializer = new XmlSerializer(typeof(Catalog));
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                serializer.Serialize(stream, catalog);
                stream.Close();
            }
        }
    }
}
