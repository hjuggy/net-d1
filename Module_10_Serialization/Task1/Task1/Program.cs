﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Task1.Models;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            SerializerBooks serializerBooks = new SerializerBooks();

            string filePath = Path.GetFullPath(Directory.GetCurrentDirectory() + @"\..\..\Resources\books.xml");

            var catalog = serializerBooks.Read(filePath);
           
            string fileName1 = Path.GetFullPath(Directory.GetCurrentDirectory() + @"\..\..\Resources\books1.xml");

            serializerBooks.Write(catalog, fileName1);
        }
    }
}
