﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Task1.Models
{
    [XmlRoot("catalog")]
    public class Catalog
    {
        [XmlIgnore]
        public DateTime Date { get; set; }

        [XmlAttribute("date")]
        public string DatePublishSerilaize
        {
            get { return Date.ToString("yyyy-MM-dd"); }
            set { Date = DateTime.Parse(value); }
        }

        [XmlElement("book")]
        public Book[] Books { get; set; }
    }
}
