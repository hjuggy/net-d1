﻿using System.Xml.Serialization;

namespace Task1.Models
{
    public enum Genre
    {
        Computer,
        Fantasy,
        Romance,
        Horror,
        [XmlEnum("Science Fiction")]
        ScienceFiction,
    }
}
