﻿using System;
using System.Xml.Serialization;

namespace Task1.Models
{
    public class Book
    {
        [XmlElement("id")]
        public string Id { get; set; }

        [XmlElement("isbn")]
        public string ISBN { get; set; }

        [XmlElement("author")]
        public string Author { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("genre")]
        public Genre Genre { get; set; }

        [XmlElement("publisher")]
        public string Publisher { get; set; }

        [XmlIgnore]
        public DateTime DatePublish { get; set; }

        [XmlElement("publish_date")]
        public string DatePublishSerilaize
        {
            get { return DatePublish.ToString("yyyy-MM-dd"); }
            set { DatePublish = DateTime.Parse(value); }
        }

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlIgnore]
        public DateTime DateRegistration { get; set; }

        [XmlElement("registration_date")]
        public string DateRegistrationSerilaize
        {
            get { return DateRegistration.ToString("yyyy-MM-dd"); }
            set { DateRegistration = DateTime.Parse(value); }
        }
    }
}
