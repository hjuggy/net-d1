﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wget;

namespace MyWget
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input URL to site:");
            string site = Console.ReadLine();
            Console.WriteLine("Enter the path to the folder (where to copy?):");
            var target = Console.ReadLine();
            var wget = new SiteLoader();
            var siteUri = new Uri(site);
            var targetUri = new Uri(target);
            wget.SiteLoading += Wget_SiteLoading;
            wget.Start(siteUri, targetUri);
            Console.WriteLine("done");
            Console.ReadKey();
        }

        private static void Wget_SiteLoading(object sender, SiteLoaderArgs e)
        {
            Console.WriteLine($"Loading \t{e.UriSite}");
        }
    }
}
