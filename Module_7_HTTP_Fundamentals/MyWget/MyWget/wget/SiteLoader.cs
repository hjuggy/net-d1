﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Handlers;

namespace wget
{
    public class SiteLoader
    {
        private List<Uri> webSites = new List<Uri>();
        public event EventHandler<SiteLoaderArgs> SiteLoading;

        public void Start(Uri siteUri, Uri targetUri)
        {
            webSites.Add(siteUri);
            Get(new ItemWork
            {
                BaseUri = siteUri,
                CarrentUri = siteUri,
                TargetFolder = targetUri,
                FileUri = new Uri(targetUri, "index.html")
            });
        }

        private void Get(ItemWork item)
        {
            var httpClient = HttpClientFactory.Create(new[] { new ProgressMessageHandler() });
            var response = httpClient.GetAsync(item.CarrentUri).Result;
            var fileName = new Uri(item.TargetFolder, item.FileUri).LocalPath;
            OnLoading(item);
            if (response.IsSuccessStatusCode)
            {
                var mimeType = response.Content.Headers.ContentType.MediaType;
                if (IsHtml(mimeType))
                {
                    GetHtmlAndLinked(response, fileName, item);
                }
                else if (IsCss(mimeType))
                {
                    GetCssAndLinked(response, fileName, item);
                }
                else if (IsScript(mimeType))
                {
                    GetScriptAndLinked(response, fileName, item);
                }
                else
                {
                    WriteFile(response, fileName);
                }
            }
            else if (response.StatusCode == HttpStatusCode.Redirect)
            {
                item.CarrentUri = response.Content.Headers.ContentLocation;
                if (!webSites.Contains(item.CarrentUri))
                {
                    webSites.Add(item.CarrentUri);
                    Get(item);
                }
            }
        }

        private void DownloadLinked(ParseUriResult parseResult, ItemWork item)
        {
            foreach (var parseItem in parseResult.Uris)
            {
                if (!webSites.Contains(parseItem.SiteUri))
                {
                    webSites.Add(parseItem.SiteUri);

                    Get(new ItemWork
                    {
                        BaseUri = item.BaseUri,
                        CarrentUri = parseItem.SiteUri,
                        FileUri = parseItem.FileUri,
                        TargetFolder = item.TargetFolder
                    });
                }
            }
        }

        private void GetCssAndLinked(HttpResponseMessage response, string fileName, ItemWork item)
        {
            var css = response.Content.ReadAsStringAsync().Result;
            var parser = new SiteParser(item.BaseUri, item.CarrentUri);
            var parseResult = parser.AnalyzeCss(css);
            WriteFile(fileName, parseResult);
            DownloadLinked(parseResult, item);
        }

        private void GetHtmlAndLinked(HttpResponseMessage response, string fileName, ItemWork item)
        {
            var html = response.Content.ReadAsStringAsync().Result;
            var parser = new SiteParser(item.BaseUri, item.CarrentUri);
            var parseResult = parser.AnalyzeHtml(html);
            WriteFile(fileName, parseResult);
            DownloadLinked(parseResult, item);
        }

        private void GetScriptAndLinked(HttpResponseMessage response, string fileName, ItemWork item)
        {
            var script = response.Content.ReadAsStringAsync().Result;
            var parser = new SiteParser(item.BaseUri, item.CarrentUri);
            var parseResult = parser.AnalyzeScript(script);
            WriteFile(fileName, parseResult);
            DownloadLinked(parseResult, item);
        }

        private static bool IsCss(string mimeType) => mimeType.Equals("text/css");

        private static bool IsHtml(string mimeType) =>
            mimeType.StartsWith("text/htm") || mimeType.StartsWith("application/xhtml");

        private static bool IsScript(string mimeType) =>
            mimeType.Equals("application/x-javascript") || mimeType.Equals("application/javascript") ||
                   mimeType.Equals("application/ecmascript") || mimeType.Equals("text/ecmascript") ||
                   mimeType.Equals("text/javascript");

        private void WriteFile(HttpResponseMessage response, string fileName)
        {
            var byteArray = response.Content.ReadAsByteArrayAsync().Result;
            new FileInfo(fileName).Directory?.Create();
            File.WriteAllBytes(fileName, byteArray);
        }

        private void WriteFile(string fileName, ParseUriResult parseResult)
        {
            new FileInfo(fileName).Directory?.Create();
            File.WriteAllText(fileName, parseResult.Payload);
        }

        protected virtual void OnLoading(ItemWork item)
        {
            var args = new SiteLoaderArgs(item);
            SiteLoading?.Invoke(this, args);
        }
    }
}