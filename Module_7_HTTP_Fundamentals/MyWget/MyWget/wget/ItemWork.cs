﻿using System;

namespace wget
{
    public class ItemWork
    {
        public Uri BaseUri { get; set; }
        public Uri CarrentUri { get; set; }
        public Uri TargetFolder { get; set; }
        public Uri FileUri { get; set; }
    }
}