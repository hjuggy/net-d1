﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace wget
{
    public class SiteParser
    {
        private readonly Uri _baseUri;
        private readonly Uri _uri;

        public SiteParser(Uri baseUri, Uri uri)
        {
            _baseUri = baseUri;
            _uri = uri;
        }

        public ParseUriResult AnalyzeCss(string css)
        {
            var parseResult = new ParseUriResult();
            var rgx = new Regex(@"uri\((" + _baseUri + @")\)[^\b;]*");
            var matches = rgx.Match(css);
            while (matches.Success)
            {
                var uri = MakeUri(matches.Groups[1].Value);
                var filepath = MakeFileSystemFriendly(uri);
                parseResult.Uris.Add(new ParseItem(uri, filepath));
                matches.NextMatch();
            }
            css = css.Replace(_baseUri.ToString(), "");
            parseResult.Payload = css;
            return parseResult;
        }

        public ParseUriResult AnalyzeHtml(string html)
        {
            var htmlDocument = new HtmlDocument { OptionFixNestedTags = true };
            htmlDocument.LoadHtml(html);
            var parseResult = new ParseUriResult();
            if (htmlDocument.DocumentNode != null)
            {
                FindLinks(htmlDocument, parseResult);
                FindCss(htmlDocument, parseResult);
                FindScript(htmlDocument, parseResult);
                var updatedHtml = new StringBuilder();
                var stringWriter = new StringWriter(updatedHtml);
                htmlDocument.DocumentNode.WriteTo(stringWriter);
                parseResult.Payload = updatedHtml.ToString();
            }
            return parseResult;
        }

        public ParseUriResult AnalyzeScript(string script)
        {
            var parseResult = new ParseUriResult();
            var matches = new Regex(@"""(" + _baseUri + @")[^""]*").Match(script);
            while (matches.Success)
            {
                var uri = MakeUri(matches.Groups[1].Value);
                var filePath = MakeFileSystemFriendly(uri);
                parseResult.Uris.Add(new ParseItem(uri, filePath));
                matches.NextMatch();
            }
            script = script.Replace(_baseUri.ToString(), "");
            parseResult.Payload = script;
            return parseResult;
        }


        private void FindCss(HtmlDocument htmlDocument, ParseUriResult parseUriResult)
        {
            var rgx = new Regex(@"uri\((" + _baseUri + @")\)[^\b;]*");
            var htmlNodes = htmlDocument.DocumentNode.SelectNodes($"//style");
            if (htmlNodes != null)
            {
                foreach (var node in htmlNodes)
                {
                    var matches = rgx.Match(node.InnerHtml);
                    while (matches.Success)
                    {
                        var uri = MakeUri(matches.Groups[1].Value);
                        var filepath = MakeFileSystemFriendly(uri);
                        parseUriResult.Uris.Add(new ParseItem(uri, filepath));
                        matches.NextMatch();
                    }
                    node.InnerHtml = node.InnerHtml.Replace(_baseUri.ToString(), "");
                }
            }
        }

        private void FindLinks(HtmlDocument htmlDocument, ParseUriResult parseUriResult)
        {
            var attributes = new [] { "href", "src"};
            foreach (var attribute in attributes)
            {
                var htmlNodes = htmlDocument.DocumentNode.SelectNodes($"//*/@{attribute}");
                if (htmlNodes != null)
                {
                    foreach (var nodeWithLink in htmlNodes)
                    {
                        var htmlAttributeValue = nodeWithLink.Attributes[attribute].Value;
                        var link = MakeUri(htmlAttributeValue);
                        if (_baseUri.IsBaseOf(link))
                        {
                            var fileUri = MakeFileSystemFriendly(link);
                            parseUriResult.Uris.Add(new ParseItem(link, fileUri));
                            var absoluteUri = new Uri(_baseUri, fileUri);
                            var relativeUri = _uri.MakeRelativeUri(absoluteUri);
                            nodeWithLink.Attributes[attribute].Value = relativeUri.ToString();
                        }
                    }
                }
            }
           
        }

        private void FindScript(HtmlDocument htmlDocument, ParseUriResult parseUriResult)
        {
            var rgx = new Regex(@"""(" + _baseUri + @"[^""]*)""");
            var htmlNodes = htmlDocument.DocumentNode.SelectNodes($"//script");
            if (htmlNodes != null)
            {
                foreach (var node in htmlNodes)
                {
                    var matches = rgx.Match(node.InnerHtml);
                    while (matches.Success)
                    {
                        var uri = MakeUri(matches.Groups[1].Value);
                        var filepath = MakeFileSystemFriendly(uri);
                        parseUriResult.Uris.Add(new ParseItem(uri, filepath));
                        matches.NextMatch();
                    }
                    node.InnerHtml = node.InnerHtml.Replace(_baseUri.ToString(), "");
                }
            }
        }

        private Uri MakeFileSystemFriendly(Uri uri)
        {
            var localPath = uri.LocalPath;
            var match = new Regex(@"^(.*/[^/]*)\.([^/\.]*)$").Match(localPath);
            string path;
            string extension;
            if (!match.Success)
            {
                path = localPath + "/index";
                extension = "html";
            }
            else
            {
                path = match.Groups[1].Value;
                extension = match.Groups[2].Value;
            }
            var query = uri.Query;
            if (query.Length > 0)
            {
                foreach (var c in Path.GetInvalidFileNameChars())
                {
                    query = query.Replace(c, '_');
                }
            }
            localPath = $"{path}{query}.{extension}";
            return new Uri(localPath.Substring(1), UriKind.Relative);
        }

        private Uri MakeUri(string url)
        {
            if (url.Contains(":"))
            {
                return new Uri(url);
            }
            var newUri = new Uri(url, UriKind.Relative);
            return newUri.IsAbsoluteUri ? newUri : new Uri(_uri, url);
        }
    }
}