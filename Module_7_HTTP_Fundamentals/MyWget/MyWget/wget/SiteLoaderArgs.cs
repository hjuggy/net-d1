﻿namespace wget
{
    public class SiteLoaderArgs
    {
        public SiteLoaderArgs(ItemWork item)
        {
            UriSite = item.CarrentUri.OriginalString;
        }
        public string UriSite { get; }
    }
}