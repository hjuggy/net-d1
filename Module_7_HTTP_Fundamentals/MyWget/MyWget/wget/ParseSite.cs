﻿using System;

namespace wget
{
    public class ParseItem
    {
        public ParseItem(Uri siteUri, Uri fileUri)
        {
            SiteUri = siteUri;
            FileUri = fileUri;
        }

        public Uri FileUri { get; set; }

        public Uri SiteUri { get; set; }
    }
}