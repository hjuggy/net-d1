﻿using System.Collections.Generic;

namespace wget
{
    public class ParseUriResult
    {
        public string Payload { get; set; }

        public ICollection<ParseItem> Uris { get; set; } = new List<ParseItem>();
    }
}