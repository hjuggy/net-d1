/*1.1.1
������� � ������� Orders ������, ������� ���� ���������� ����� 6 ��� 1998 ���� (������� ShippedDate) 
������������ � ������� ���������� � ShipVia >= 2. ������ ������ ���������� ������ ������� OrderID, 
ShippedDate � ShipVia.
*/
SELECT dbo.Orders.OrderID, dbo.Orders.ShippedDate, dbo.Orders.ShipVia  FROM dbo.Orders
WHERE ShippedDate>='1998-05-06' --AND ShipVia=2

/*1.1.2
�������� ������, ������� ������� ������ �������������� ������ �� ������� Orders. � ����������� ������� 
���������� ��� ������� ShippedDate ������ �������� NULL ������ �Not Shipped� (������������ ��������� 
������� CAS�). ������ ������ ���������� ������ ������� OrderID � ShippedDate.
*/
SELECT	OrderID
		,ShippedDate =
			CASE 
			WHEN ShippedDate IS NULL THEN 'Not Shipped'
			END 
FROM dbo.Orders
WHERE ShippedDate IS NULL

/*1.1.3
������� � ������� Orders ������, ������� ���� ���������� ����� 6 ��� 1998 ���� (ShippedDate) �� ������� ��� 
���� ��� ������� ��� �� ����������. � ������� ������ ������������ ������ ������� OrderID (������������� � Order 
Number) � ShippedDate (������������� � Shipped Date). � ����������� ������� ���������� ��� ������� ShippedDate 
������ �������� NULL ������ �Not Shipped�, ��� ��������� �������� ���������� ���� � ������� �� ���������.
*/
SELECT dbo.Orders.OrderID as [Order Number],
CASE
	WHEN ShippedDate IS NOT NULL THEN CAST(ShippedDate AS char)
	WHEN ShippedDate IS NULL THEN 'Not Shipped'
END as [Shipped Date]
FROM dbo.Orders
WHERE ShippedDate>'1998-05-06' OR ShippedDate IS NULL

/*1.2.1
������� �� ������� Customers ���� ����������, ����������� � USA � Canada.
������ ������� � ������ ������� ��������� IN. ���������� ������� � ������ 
������������ � ��������� ������ � ����������� �������. ����������� ���������� 
������� �� ����� ���������� � �� ����� ����������.
*/
select ContactName, Country from Customers
where Country in('USA','Canada')
order by Country, ContactName

/*1.2.2
������� �� ������� Customers ���� ����������, �� ����������� � USA � Canada.
������ ������� � ������� ��������� IN. ���������� ������� � ������ ������������ � 
��������� ������ � ����������� �������. ����������� ���������� ������� �� ����� ����������.
*/
select ContactName, Country from Customers
where Country not in('USA','Canada')
order by ContactName

/*1.2.3
������� �� ������� Customers ��� ������, � ������� ��������� ���������. ������ ������ ���� ��������� 
������ ���� ��� � ������ ������������ �� ��������. �� ������������ ����������� GROUP BY.
���������� ������ ���� ������� � ����������� �������.
*/
select distinct Country from Customers
order by Country desc

/*1.3.1
������� ��� ������ (OrderID) �� ������� Order Details (������ �� ������ �����������), ��� ����������� �������� 
� ����������� �� 3 �� 10 ������������ � ��� ������� Quantity � ������� Order Details. ������������ �������� BETWEEN.
������ ������ ���������� ������ ������� OrderID.
*/
select distinct OrderID from [Order Details]
where Quantity between 3 and 10  

/*1.3.2
������� ���� ���������� �� ������� Customers, � ������� �������� ������ ���������� �� ����� �� ��������� b � g. 
������������ �������� BETWEEN. ���������, ��� � ���������� ������� �������� Germany. ������ ������ ���������� ������ 
������� CustomerID � Country � ������������ �� Country.
*/
select CustomerID, Country from Customers
where Country between 'b' and 'h' 
order by Country 

/*1.3.3
������� ���� ���������� �� ������� Customers, � ������� �������� ������ ���������� �� ����� �� ��������� b � g,
�� ��������� �������� BETWEEN.
*/
select CustomerID, Country from Customers
where Country >= 'b' and Country <='g' 


/*1.4.1
� ������� Products ����� ��� �������� (������� ProductName), ��� ����������� ��������� 'chocolade'. ��������,
��� � ��������� 'chocolade' ����� ���� �������� ���� ����� 'c' � �������� - ����� ��� ��������, ������� 
������������� ����� �������.
*/
select ProductName from Products
where ProductName like '%ho%olade' 

/*2.1.1
����� ����� ����� ���� ������� �� ������� Order Details � ������ ���������� ����������� ������� � ������ �� ���.
����������� ������� ������ ���� ���� ������ � ����� �������� � ��������� ������� 'Totals'.
*/
select round(sum(UnitPrice*Quantity*(1-Discount)),2) as Totals from [Order Details]

/*2.1.2
�� ������� Orders ����� ���������� �������, ������� ��� �� ���� ���������� (�.�. � ������� ShippedDate ��� �������� 
���� ��������). ������������ ��� ���� ������� ������ �������� COUNT. �� ������������ ����������� WHERE � GROUP.
*/
select count(ShippedDate) from Orders

/*2.1.3
�� ������� Orders ����� ���������� ��������� ����������� (CustomerID), ��������� ������. ������������ ������� COUNT 
� �� ������������ ����������� WHERE � GROUP.
*/
select count(distinct CustomerID) from Orders

/*2.2.1
�� ������� Orders ����� ���������� ������� � ������������ �� �����. � ����������� ������� ���� ���������� ��� ������� 
c ���������� Year � Total. �������� ����������� ������, ������� ��������� ���������� ���� �������.
*/
select DATEPART(year, OrderDate) as 'Year', COUNT('Year') as Total from Orders
group by DATEPART(year, OrderDate)

/*2.2.2
�� ������� Orders ����� ���������� �������, c�������� ������ ���������. ����� ��� ���������� �������� � ��� ����� ������
� ������� Orders, ��� � ������� EmployeeID ������ �������� ��� ������� ��������. � ����������� ������� ���� ���������� ������� 
� ������ �������� (������ ������������� ��� ���������� ������������� LastName & FirstName. ��� ������ LastName & FirstName ������ 
���� �������� ��������� �������� � ������� ��������� �������. ����� �������� ������ ������ ������������ ����������� �� EmployeeID.)
� ��������� ������� �Seller� � ������� c ����������� ������� ���������� � ��������� 'Amount'. ���������� ������� ������ ���� 
����������� �� �������� ���������� �������.
*/
select concat(Employees.LastName,' ', Employees.FirstName) as Seller, OrdersGroup.Amount
from Employees RIGHT OUTER JOIN (select Orders.EmployeeID, count(Orders.EmployeeID) as Amount
from   Orders 
group by Orders.EmployeeID) as OrdersGroup
on Employees.EmployeeID = OrdersGroup.EmployeeID
order by OrdersGroup.Amount desc

/*2.2.3
�� ������� Orders ����� ���������� �������, ��������� ������ ��������� � ��� ������� ����������. ���������� ����������
��� ������ ��� �������, ��������� � 1998 ����.
*/
select CustomerID, EmployeeID, count(CustomerID) as Amount from Orders
where DATEPART(year, OrderDate) = 1998
group by  EmployeeID, CustomerID 

/*2.2.4
����� ����������� � ���������, ������� ����� � ����� ������. ���� � ������ ����� ������ ���� ��� ��������� ���������, 
��� ������ ���� ��� ��������� �����������, �� ���������� � ����� ���������� � ��������� �� ������ �������� � �������������� �����.
�� ������������ ����������� JOIN.
*/
select 
	Employees.City
	,Customers.City
	,Employees.EmployeeID
	,Customers.CustomerID
	from Employees, Customers
where Employees.City = Customers.City 

/*2.2.5
����� ���� �����������, ������� ����� � ����� ������.
*/
select Customers.ContactName, Customers.City 
from Customers RIGHT OUTER JOIN (select 
	Customers.City as City	
	,count(Customers.CustomerID) as CountCustomerID
from Customers
group by Customers.City
having count(Customers.CustomerID)>1) as CiteMoreOneCustome
on Customers.City = CiteMoreOneCustome.City

/*2.3.1
���������� ���������, ������� ����������� ������ 'Western' (������� Region)
*/
select distinct Employees.LastName, Employees.FirstName  from Employees 
join EmployeeTerritories on Employees.EmployeeID = EmployeeTerritories.EmployeeID
join Territories on EmployeeTerritories.TerritoryID = Territories.TerritoryID
join Region on Region.RegionID = Territories.RegionID
where Region.RegionDescription  = 'Western'

/*2.3.2
������ � ����������� ������� ����� ���� ���������� �� ������� Customers � ��������� 
���������� �� ������� �� ������� Orders. ������� �� ��������, ��� � ��������� ���������� 
��� �������, �� ��� ����� ������ ���� �������� � ����������� �������. ����������� ����������
������� �� ����������� ���������� �������.
*/
select Customers.ContactName, count(Orders.CustomerID) as Amount from Customers
left join Orders 
on Customers.CustomerID = Orders.CustomerID
group by Customers.ContactName
 order by Amount desc

 /*2.4.1
 ������ ���� ����������� (������� CompanyName � ������� Suppliers), � ������� ��� ���� �� ������ �������� 
 �� ������ (UnitsInStock � ������� Products ����� 0). ������������ ��������� SELECT ��� 
 ����� ������� � �������������� ��������� IN.
 */
  select CompanyName from Suppliers
 where SupplierID in (select SupplierID from Products
  where UnitsInStock=0)

  /*2.4.2
  ������ ���� ���������, ������� ����� ����� 150 �������. ������������ ��������� SELECT
  */
select Employees.LastName, Employees.FirstName, Employees.EmployeeID from Employees 
right outer join (select Orders.EmployeeID as EmployeeID , count(Orders.EmployeeID) as Amount from Orders
group by EmployeeID
having count(EmployeeID)>150) as EmployeeMore150Orders
on Employees.EmployeeID = EmployeeMore150Orders.EmployeeID

/*2.4.3
������ ���� ���������� (������� Customers), ������� �� ����� �� ������ ������ (��������� �� ������� Orders). ������������ �������� EXISTS.
*/
select Customers.CustomerID from Customers
where not exists (select distinct Orders.CustomerID from Orders
				  where Orders.CustomerID = Customers.CustomerID)

