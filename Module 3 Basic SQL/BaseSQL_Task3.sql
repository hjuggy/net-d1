
GO
CREATE TABLE "CreditCard" (
	"CardID" "int" IDENTITY (1, 1) NOT NULL ,
	"Number" nvarchar (16) NOT NULL ,
	"ExpirateDate" "datetime" NOT NULL ,
	"CardHolder" nvarchar (60) NOT NULL ,	
	"EmployeeID" "int" NULL ,
	CONSTRAINT "PK_CreditCard" PRIMARY KEY  CLUSTERED 
	(
		"CardID"
	),
	CONSTRAINT "FK_CreditCard_Employees" FOREIGN KEY 
	(
		"EmployeeID"
	) REFERENCES "dbo"."Employees" (
		"EmployeeID"
	),
	CONSTRAINT "CK_ExpirateDate" CHECK (ExpirateDate > getdate())
)